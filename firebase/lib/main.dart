import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'auth/auth_service.dart';
import 'models/UserManager.dart';
import 'models/hive_config.dart';
import 'models/settings.dart';
import 'screens/login.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await HiveConfig.start();
  await Firebase.initializeApp();

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => AuthService()),
        //ChangeNotifierProvider(create: (context) => ContaRepository()),
        ChangeNotifierProvider(create: (context) => AppSettings()),
        ChangeNotifierProvider(create: (context) => UserManager()),
      ],
      child: Teste4(),
    ),
  );
}

//Classe necessária para o funcionamento da interface, o nome é aleatório.
class Teste4 extends StatelessWidget {
  const Teste4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "teste",
      theme: ThemeData(primaryColor: Color(0xFF7E9CFF)),
      home: MyLogin(),
      debugShowCheckedModeBanner: false,
    );
  }
}