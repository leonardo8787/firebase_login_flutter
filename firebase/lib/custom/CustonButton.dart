import 'package:flutter/material.dart';

class CustomButtom extends StatelessWidget {
  final String texto;
  final VoidCallback onClick;

  const CustomButtom({
    Key? key,
    required this.texto,
    required this.onClick,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(20, 5, 20, 0),
      child: Container(
        height: 35,
        width: MediaQuery.of(context).size.width,
        child: SizedBox.expand(
          child: TextButton(
              onPressed: onClick,
              child: Text(texto,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                    fontSize: 18,
                  ))),
        ),
      ),
    );
  }
}