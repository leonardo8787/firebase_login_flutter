import 'package:flutter/material.dart';

class CustomFormField extends StatelessWidget {

  final String? hint;
  final String? label;
  final String? Function(String?)? validacao;
  final TextInputType? teclado;
  final TextEditingController? controller;

  CustomFormField({
    Key? key,
    this.hint,
    this.label,
    this.validacao,
    this.teclado,
    this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: new ThemeData(
        colorScheme:
        ThemeData().colorScheme.copyWith(primary: Colors.black87),
      ),
      child: Padding(
          padding: const EdgeInsets.fromLTRB(20, 5, 20, 0),
          child: Container(
            child: Padding(
              padding: EdgeInsets.fromLTRB(10, 0, 10, 0),
              child: TextFormField(
                controller: controller,
                validator: validacao,
                keyboardType: teclado,
                autofocus: false,
                style: TextStyle(fontSize: 15.0, color: Colors.black87),
                decoration: InputDecoration(
                  icon: Icon(Icons.person),
                  border: InputBorder.none,
                  hintText: hint,
                  labelText: label,
                  filled: true,
                  fillColor: Colors.white,
                ),
              ),
            ),
          ),
      ),
    );
  }
}


