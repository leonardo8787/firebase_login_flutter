import 'package:firebase/custom/colors.dart';
import 'package:flutter/material.dart';

import 'dadosperfil.dart';
import 'home.dart';
import 'login.dart';

class Navigationbar extends StatelessWidget {
  const Navigationbar({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 70,
      padding: EdgeInsets.only(
        left: kDefaultPadding * 2,
        right: kDefaultPadding * 2,
      ),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
            offset: Offset(0, -10),
            blurRadius: 35,
            color: kPrimaryColor.withOpacity(0.38))
      ]),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          IconButton(
            icon: const Icon(
              Icons.logout,
              color: kPrimaryColor,
              size: 35,
            ),
            onPressed: () {
              Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => MyLogin()));
            },
          ),
          IconButton(
            icon: const Icon(
              Icons.home,
              size: 35,
              color: kPrimaryColor,
            ),
            onPressed: () {
              Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => MyHome()));
            },
          ),
          IconButton(
            icon: const Icon(
              Icons.account_circle,
              size: 35,
              color: kPrimaryColor,
            ),
            onPressed: () {
              Navigator.push(context,MaterialPageRoute(builder: (BuildContext context) => Perfil()));
            },
          ),
        ],
      ),
    );
  }
}
