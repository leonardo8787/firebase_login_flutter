import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../models/UserManager.dart';
import 'navigationbat.dart';

class Perfil extends StatefulWidget {
  const Perfil({ Key? key }) : super(key: key);

  @override
  _PerfilState createState() => _PerfilState();
}

class _PerfilState extends State<Perfil> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body:Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/register.png'), fit: BoxFit.cover),
        ),
      child: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 50,),
                Center(
                  child: CircleAvatar(
                    radius: 90,
                  ),
                ),
                SizedBox(height: 30,),
                Center(child: Text(" Dados do usuário ", style: 
                TextStyle(color: Colors.black87,fontSize: 20),),),
                Divider(
                  height: 10,
                  color: Colors.black,
                ),
                Consumer<UserManager>(
                  builder: (_, userManager, __) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Center(
                          child: Padding( 
                            padding: EdgeInsets.all(16.0),
                            child: Text("nome: " + userManager.user.nome!),
                          ),
                        ),
                        Center(
                          child: Padding( 
                            padding: EdgeInsets.all(16.0),
                            child: Text("email: " + userManager.user.email!),
                          ),
                        ),
                        SizedBox(height: 400,),
                      ],
                    );
                  }
                ),
              ],
            ),
          ],
          ),
        ),
        ),
      ),
      bottomNavigationBar: Navigationbar(),
    );
  }
}