import 'package:cloud_firestore/cloud_firestore.dart';

class Usuario {
  String? id;
  String? email;
  String? nome;

  Usuario([
    this.email = '',
    this.nome = '',
  ]){
    this.nome = nome;
    this.email = email;
  }
  Usuario.fromDocument(DocumentSnapshot document){
    Map data = document.data() as Map;
    this.nome = data['Nome'];
    this.email = data['email'];
  }

  DocumentReference get firestoreRef => FirebaseFirestore.instance.collection("users").doc(this.id);
  
  Future salvaDados() async{
    await firestoreRef.set(toMap());
  }

  Map<String, dynamic> toMap() {
    return {
      'email': this.email,
      'Nome': this.nome,
    };
  }

  @override
  String toString() {
    return 'Usuario(email: $email, nome: $nome)';
  }
}