import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase/models/Usuario.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import '../auth/auth_service.dart';

class UserManager extends ChangeNotifier{
  Usuario user = Usuario();
  bool loading = false;

registrar(Usuario user, String senha) async {
   loading = true;
    try {
       final UserCredential result =
          await FirebaseAuth.instance.createUserWithEmailAndPassword(
              email: user.email!, password: senha);
    
    user.id = result.user!.uid;
    this.user = user;
    await user.salvaDados();
     
    } on AuthException catch (e) {
      print(e);
    }
    loading = false;
  }
  
  login(String email, String senha) async {
    loading = true;
    try {
      final UserCredential userLog = await FirebaseAuth.instance.signInWithEmailAndPassword(email: email, password: senha);
      loading = false;
      DocumentSnapshot document = await FirebaseFirestore.instance.collection("users").doc(userLog.user!.uid).get();
      user = Usuario.fromDocument(document);
    } on FirebaseAuthException catch (e) {
      if (e.code == 'Usuário não encontrado') {
        throw AuthException('Email não encontrado. Cadastre-se.');
      } else if (e.code == 'Senha não encontrada') {
        throw AuthException('Senha incorreta. Tente novamente');
      }
    }
  }
} 
